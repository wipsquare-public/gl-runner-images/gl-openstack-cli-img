FROM python:alpine
RUN apk add \
        build-base \
        git \
        libffi-dev \
        openssh-client \
        rsync \
        yq \
        curl
RUN pip install --no-cache-dir --no-compile python-heatclient python-openstackclient openstacksdk PyYAML Jinja2 net-tools
CMD [ "openstack", "--version" ]